/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>

#include "libgimpbase/gimpbase.h"
#include "libgimpmath/gimpmath.h"
#include "libgimpcolor/gimpcolor.h"

#include "tools-types.h"

#include "config/gimpdisplayconfig.h"
#include "config/gimpguiconfig.h"

#include "core/gimp.h"
#include "core/gimp-utils.h"
#include "core/gimpdrawable.h"
#include "core/gimpdynamics.h"
#include "core/gimpdynamicsoutput.h"
#include "core/gimperror.h"
#include "core/gimpimage.h"
#include "core/gimpimage-symmetry.h"
#include "core/gimplayer.h"
#include "core/gimppaintinfo.h"
#include "core/gimpprojection.h"
#include "core/gimpsymmetry.h"
#include "core/gimptoolinfo.h"
#include "core/gimpimage-pick-item.h"

#include "paint/gimppaintcore.h"
#include "paint/gimppaintoptions.h"
#include "paint/gimppaintbrush.h"

#include "widgets/gimpdevices.h"
#include "widgets/gimpwidgets-utils.h"

#include "display/gimpdisplay.h"
#include "display/gimpdisplayshell.h"
#include "display/gimpdisplayshell-appearance.h"
#include "display/gimpdisplayshell-selection.h"
#include "display/gimpdisplayshell-utils.h"
#include "display/gimpcanvashandle.h"

#include "gimpcoloroptions.h"
#include "gimperasertool.h"
#include "gimpsmudgetool.h"
#include "gimppaintoptions-gui.h"
#include "gimppainttool.h"
#include "gimppainttool-paint.h"
#include "gimpsourcetool.h"
#include "gimptoolcontrol.h"
#include "gimptools-utils.h"

#include "vectors/gimpvectors.h"

#include "gimp-intl.h"

typedef struct
  {
    gdouble x;
    gdouble y;
  } point;

#define FINDER_THRESHOLD 16
#define FINDER_SIZE 16
#define CORE_SIZE 4
#define PICK_PATH_PROXIMITY 8
#define CIRCLE_DASH_COUNT 8
#define CIRCLE_DASH_SPACE 0.5
#define EPSILON 1e-6

static void   gimp_paint_tool_constructed    (GObject               *object);
static void   gimp_paint_tool_finalize       (GObject               *object);

static void   gimp_paint_tool_control        (GimpTool              *tool,
                                              GimpToolAction         action,
                                              GimpDisplay           *display);
static void   gimp_paint_tool_button_press   (GimpTool              *tool,
                                              const GimpCoords      *coords,
                                              guint32                time,
                                              GdkModifierType        state,
                                              GimpButtonPressType    press_type,
                                              GimpDisplay           *display);
static void   gimp_paint_tool_button_release (GimpTool              *tool,
                                              const GimpCoords      *coords,
                                              guint32                time,
                                              GdkModifierType        state,
                                              GimpButtonReleaseType  release_type,
                                              GimpDisplay           *display);
static void   gimp_paint_tool_motion         (GimpTool              *tool,
                                              const GimpCoords      *coords,
                                              guint32                time,
                                              GdkModifierType        state,
                                              GimpDisplay           *display);
static void   gimp_paint_tool_modifier_key   (GimpTool              *tool,
                                              GdkModifierType        key,
                                              gboolean               press,
                                              GdkModifierType        state,
                                              GimpDisplay           *display);
static void   gimp_paint_tool_cursor_update  (GimpTool              *tool,
                                              const GimpCoords      *coords,
                                              GdkModifierType        state,
                                              GimpDisplay           *display);
static void   gimp_paint_tool_oper_update    (GimpTool              *tool,
                                              const GimpCoords      *coords,
                                              GdkModifierType        state,
                                              gboolean               proximity,
                                              GimpDisplay           *display);

static void   gimp_paint_tool_draw           (GimpDrawTool          *draw_tool);

static void
          gimp_paint_tool_real_paint_prepare (GimpPaintTool         *paint_tool,
                                              GimpDisplay           *display);

static GimpCanvasItem *
              gimp_paint_tool_get_outline    (GimpPaintTool         *paint_tool,
                                              GimpDisplay           *display,
                                              gdouble                x,
                                              gdouble                y);

static gboolean  gimp_paint_tool_check_alpha (GimpPaintTool         *paint_tool,
                                              GimpDrawable          *drawable,
                                              GimpDisplay           *display,
                                              GError               **error);

static void   gimp_paint_tool_hard_notify    (GimpPaintOptions      *options,
                                              const GParamSpec      *pspec,
                                              GimpPaintTool         *paint_tool);
static void   gimp_paint_tool_cursor_notify  (GimpDisplayConfig     *config,
                                              GParamSpec            *pspec,
                                              GimpPaintTool         *paint_tool);

static void   gimp_draw_brush_cursor         (GimpDrawTool          *draw_tool,
                                              GimpCanvasItem        *outline);

static void   gimp_draw_arc_circle           (GimpDrawTool          *draw_tool,
                                              gint                   size,
                                              gint                   arc_count,
                                              gdouble                spacing,
                                              gdouble                offset);

static void   gimp_paintbrush_dab            (GimpPaintTool         *paint_tool,
                                              guint32                time);

static point      circular_random_point      (gdouble                radius);

static gdouble    get_tilt_angle             (const GimpCoords      *coords);

static void       draw_brush_circle          (GimpDrawTool          *draw_tool,
                                              gint                   brush_size);

static gboolean   should_locate              (GimpDrawTool          *draw_tool,
                                              GimpDisplayShell      *shell,
                                              gint                   small_brush_size);

static void       draw_circle                (GimpDrawTool          *draw_tool,
                                              gint                   size,
                                              gboolean               filled);

static gdouble    get_dynamic_brush_size     (GimpDrawTool          *draw_tool);

G_DEFINE_TYPE (GimpPaintTool, gimp_paint_tool, GIMP_TYPE_COLOR_TOOL)

#define parent_class gimp_paint_tool_parent_class


static void
gimp_paint_tool_class_init (GimpPaintToolClass *klass)
{
  GObjectClass      *object_class    = G_OBJECT_CLASS (klass);
  GimpToolClass     *tool_class      = GIMP_TOOL_CLASS (klass);
  GimpDrawToolClass *draw_tool_class = GIMP_DRAW_TOOL_CLASS (klass);

  object_class->constructed  = gimp_paint_tool_constructed;
  object_class->finalize     = gimp_paint_tool_finalize;

  tool_class->control        = gimp_paint_tool_control;
  tool_class->button_press   = gimp_paint_tool_button_press;
  tool_class->button_release = gimp_paint_tool_button_release;
  tool_class->motion         = gimp_paint_tool_motion;
  tool_class->modifier_key   = gimp_paint_tool_modifier_key;
  tool_class->cursor_update  = gimp_paint_tool_cursor_update;
  tool_class->oper_update    = gimp_paint_tool_oper_update;

  draw_tool_class->draw      = gimp_paint_tool_draw;

  klass->paint_prepare       = gimp_paint_tool_real_paint_prepare;
}

static void
gimp_paint_tool_init (GimpPaintTool *paint_tool)
{
  GimpTool *tool = GIMP_TOOL (paint_tool);

  gimp_tool_control_set_motion_mode    (tool->control, GIMP_MOTION_MODE_EXACT);
  gimp_tool_control_set_scroll_lock    (tool->control, TRUE);
  gimp_tool_control_set_action_opacity (tool->control,
                                        "context-opacity-set");

  paint_tool->active          = TRUE;
  paint_tool->pick_colors     = FALSE;
  paint_tool->can_multi_paint = FALSE;
  paint_tool->draw_line       = FALSE;
  paint_tool->erase           = FALSE;
  paint_tool->has_alpha       = FALSE;

  paint_tool->show_cursor   = TRUE;
  paint_tool->draw_brush    = TRUE;
  paint_tool->snap_brush    = FALSE;
  paint_tool->draw_fallback = FALSE;
  paint_tool->fallback_size = 0.0;
  paint_tool->draw_circle   = FALSE;
  paint_tool->circle_size   = 0.0;

  paint_tool->status      = _("Click to paint");
  paint_tool->status_line = _("Click to draw the line");
  paint_tool->status_ctrl = _("%s to pick a color");
  paint_tool->status_alt  = _("%s to toggle erase paint mode");

  paint_tool->core        = NULL;
}

static void
gimp_paint_tool_constructed (GObject *object)
{
  GimpTool          *tool       = GIMP_TOOL (object);
  GimpPaintTool     *paint_tool = GIMP_PAINT_TOOL (object);
  GimpPaintOptions  *options    = GIMP_PAINT_TOOL_GET_OPTIONS (tool);
  GimpDisplayConfig *display_config;
  GimpPaintInfo     *paint_info;

  G_OBJECT_CLASS (parent_class)->constructed (object);

  gimp_assert (GIMP_IS_TOOL_INFO (tool->tool_info));
  gimp_assert (GIMP_IS_PAINT_INFO (tool->tool_info->paint_info));

  display_config = GIMP_DISPLAY_CONFIG (tool->tool_info->gimp->config);

  paint_info = tool->tool_info->paint_info;

  gimp_assert (g_type_is_a (paint_info->paint_type, GIMP_TYPE_PAINT_CORE));

  paint_tool->core = g_object_new (paint_info->paint_type,
                                   "undo-desc", paint_info->blurb,
                                   NULL);

  g_signal_connect_object (options, "notify::hard",
                           G_CALLBACK (gimp_paint_tool_hard_notify),
                           paint_tool, 0);

  gimp_paint_tool_hard_notify (options, NULL, paint_tool);

  paint_tool->show_cursor = display_config->show_paint_tool_cursor;
  paint_tool->draw_brush  = display_config->show_brush_outline;
  paint_tool->snap_brush  = display_config->snap_brush_outline;

  g_signal_connect_object (display_config, "notify::show-paint-tool-cursor",
                           G_CALLBACK (gimp_paint_tool_cursor_notify),
                           paint_tool, 0);
  g_signal_connect_object (display_config, "notify::show-brush-outline",
                           G_CALLBACK (gimp_paint_tool_cursor_notify),
                           paint_tool, 0);
  g_signal_connect_object (display_config, "notify::snap-brush-outline",
                           G_CALLBACK (gimp_paint_tool_cursor_notify),
                           paint_tool, 0);
}

static void
gimp_paint_tool_finalize (GObject *object)
{
  GimpPaintTool *paint_tool = GIMP_PAINT_TOOL (object);

  if (paint_tool->core)
    {
      g_object_unref (paint_tool->core);
      paint_tool->core = NULL;
    }

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gimp_paint_tool_control (GimpTool       *tool,
                         GimpToolAction  action,
                         GimpDisplay    *display)
{
  GimpPaintTool *paint_tool = GIMP_PAINT_TOOL (tool);

  switch (action)
    {
    case GIMP_TOOL_ACTION_PAUSE:
    case GIMP_TOOL_ACTION_RESUME:
      break;

    case GIMP_TOOL_ACTION_HALT:
      gimp_paint_core_cleanup (paint_tool->core);
      break;

    case GIMP_TOOL_ACTION_COMMIT:
      break;
    }

  GIMP_TOOL_CLASS (parent_class)->control (tool, action, display);
}

static void
gimp_paint_tool_button_press (GimpTool            *tool,
                              const GimpCoords    *coords,
                              guint32              time,
                              GdkModifierType      state,
                              GimpButtonPressType  press_type,
                              GimpDisplay         *display)
{
  GimpDrawTool     *draw_tool  = GIMP_DRAW_TOOL (tool);
  GimpPaintTool    *paint_tool = GIMP_PAINT_TOOL (tool);
  GimpPaintCore    *paint_core = paint_tool->core;
  GimpPaintOptions *options    = GIMP_PAINT_TOOL_GET_OPTIONS (tool);
  GimpGuiConfig    *config     = GIMP_GUI_CONFIG (display->gimp->config);
  GimpDisplayShell *shell      = gimp_display_get_shell (display);
  GimpImage        *image      = gimp_display_get_image (display);
  GList            *drawables;
  GList            *iter;
  gboolean          constrain;
  GError           *error = NULL;
  GimpVectors      *vectors;

  if (gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
    {
      GIMP_TOOL_CLASS (parent_class)->button_press (tool, coords, time, state,
                                                    press_type, display);
      return;
    }

  drawables  = gimp_image_get_selected_drawables (image);

  if (drawables == NULL)
    {
      gimp_tool_message_literal (tool, display,
                                 _("No selected drawables."));

      return;
    }
  else if (! paint_tool->can_multi_paint)
    {
      if (g_list_length (drawables) != 1)
        {
          gimp_tool_message_literal (tool, display,
                                     _("Cannot paint on multiple layers. Select only one layer."));

          g_list_free (drawables);
          return;
        }
    }

  for (iter = drawables; iter; iter = iter->next)
    {
      GimpDrawable *drawable    = iter->data;
      GimpItem     *locked_item = NULL;

      if (gimp_viewable_get_children (GIMP_VIEWABLE (drawable)))
        {
          gimp_tool_message_literal (tool, display,
                                     _("Cannot paint on layer groups."));
          g_list_free (drawables);

          return;
        }

      if (gimp_item_is_content_locked (GIMP_ITEM (drawable), &locked_item))
        {
          gboolean constrain_only;

          /* Allow pixel-locked layers to be set as sources */
          constrain_only = (state & gimp_get_constrain_behavior_mask () &&
                            ! (state & gimp_get_extend_selection_mask ()));
          if (! (GIMP_IS_SOURCE_TOOL (tool) && constrain_only))
            {
              gimp_tool_message_literal (tool, display,
                                         _("The selected item's pixels are locked."));
              gimp_tools_blink_lock_box (display->gimp, locked_item);
              g_list_free (drawables);

              return;
            }
        }

      if (! gimp_paint_tool_check_alpha (paint_tool, drawable, display, &error))
        {
          GtkWidget *options_gui;
          GtkWidget *mode_box;

          gimp_tool_message_literal (tool, display, error->message);

          options_gui = gimp_tools_get_tool_options_gui (GIMP_TOOL_OPTIONS (options));
          mode_box    = gimp_paint_options_gui_get_paint_mode_box (options_gui);

          if (gtk_widget_is_sensitive (mode_box))
            {
              gimp_tools_show_tool_options (display->gimp);
              gimp_widget_blink (mode_box);
            }

          g_clear_error (&error);
          g_list_free (drawables);

          return;
        }

      if (! gimp_item_is_visible (GIMP_ITEM (drawable)) &&
          ! config->edit_non_visible)
        {
          gimp_tool_message_literal (tool, display,
                                     _("A selected layer is not visible."));
          g_list_free (drawables);

          return;
        }
    }

  if (gimp_draw_tool_is_active (draw_tool))
    gimp_draw_tool_stop (draw_tool);

  if (tool->display            &&
      tool->display != display &&
      gimp_display_get_image (tool->display) == image)
    {
      /*  if this is a different display, but the same image, HACK around
       *  in tool internals AFTER stopping the current draw_tool, so
       *  straight line drawing works across different views of the
       *  same image.
       */

      tool->display = display;
    }

  if (options->brush_snap_to_active_path)
    {
      GimpVectors *active_vector    = NULL;
      GimpItem    *item             = NULL;
      gint         active_vector_id = -1;
      gint         picked_vector_id = -2;

      if (g_list_length (gimp_image_get_selected_vectors (image)) == 1)
        active_vector = gimp_image_get_selected_vectors (image)->data;

      if (active_vector)
        {
          item             = GIMP_ITEM (active_vector);
          active_vector_id = gimp_item_get_id (item);
        }

      vectors = gimp_image_pick_vectors (image,
                                        coords->x, coords->y,
                                        FUNSCALEX (shell, PICK_PATH_PROXIMITY),
                                        FUNSCALEY (shell, PICK_PATH_PROXIMITY));
      if (vectors)
        {
          GList *new_selected_vectors = g_list_prepend (NULL, vectors);

          item                        = GIMP_ITEM (vectors);
          picked_vector_id            = gimp_item_get_id (item);

          gimp_image_set_selected_vectors (image, new_selected_vectors);
          g_list_free (new_selected_vectors);

          gimp_display_shell_set_snap_to_vectors (shell, TRUE);

          /* avoid painting when picking a new path */
          if (picked_vector_id != active_vector_id) return;
        }
    }

  constrain = (state & gimp_get_constrain_behavior_mask ()) != 0;

  /* store the coords when the tablet is tapped for the dab procedure */
  gimp_paint_core_set_dab_coords (paint_core, coords);

  if (! gimp_paint_tool_paint_start (paint_tool,
                                     display, coords, time, constrain, FALSE,
                                     &error))
    {
      gimp_tool_message_literal (tool, display, error->message);
      g_clear_error (&error);
      return;
    }

  tool->display   = display;
  tool->drawables = drawables;

  /*  pause the current selection  */
  gimp_display_shell_selection_pause (shell);

  gimp_draw_tool_start (draw_tool, display);

  gimp_tool_control_activate (tool->control);
}

static void
gimp_paint_tool_button_release (GimpTool              *tool,
                                const GimpCoords      *coords,
                                guint32                time,
                                GdkModifierType        state,
                                GimpButtonReleaseType  release_type,
                                GimpDisplay           *display)
{
  GimpPaintTool    *paint_tool    = GIMP_PAINT_TOOL (tool);
  GimpPaintCore    *paint_core    = paint_tool->core;
  GimpDisplayShell *shell         = gimp_display_get_shell (display);
  GimpImage        *image         = gimp_display_get_image (display);
  GimpPaintOptions *options       = GIMP_PAINT_TOOL_GET_OPTIONS (tool);
  gboolean          use_brush_dab = options->brush_dab_options->use_brush_dab;
  gdouble           dab_threshold = options->brush_dab_options->dab_threshold;
  gboolean          cancel;

  if (gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
    {
      GIMP_TOOL_CLASS (parent_class)->button_release (tool, coords, time,
                                                      state, release_type,
                                                      display);
      return;
    }

  cancel = (release_type == GIMP_BUTTON_RELEASE_CANCEL);


  if (use_brush_dab && (paint_core->pixel_dist < dab_threshold))
    {
      /* cancel the previous stroke */
      gimp_paint_tool_paint_end (paint_tool, time, TRUE);

      /* restart the paint tool */
      gimp_paint_tool_paint_start (paint_tool,
                                   display, coords, time, FALSE, TRUE, NULL);

      /* paint a dab */
      gimp_paintbrush_dab (paint_tool, time);
    }

  gimp_paint_tool_paint_end (paint_tool, time, cancel);

  gimp_tool_control_halt (tool->control);

  gimp_draw_tool_pause (GIMP_DRAW_TOOL (tool));

  /*  resume the current selection  */
  gimp_display_shell_selection_resume (shell);

  gimp_image_flush (image);

  gimp_draw_tool_resume (GIMP_DRAW_TOOL (tool));
}

static void
gimp_paint_tool_motion (GimpTool         *tool,
                        const GimpCoords *coords,
                        guint32           time,
                        GdkModifierType   state,
                        GimpDisplay      *display)
{
  GimpPaintTool *paint_tool = GIMP_PAINT_TOOL (tool);
  GIMP_TOOL_CLASS (parent_class)->motion (tool, coords, time, state, display);

  if (gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
    return;

  if (! paint_tool->snap_brush)
    gimp_draw_tool_pause  (GIMP_DRAW_TOOL (tool));

  gimp_paint_tool_paint_motion (paint_tool, coords, time);

  if (! paint_tool->snap_brush)
    gimp_draw_tool_resume (GIMP_DRAW_TOOL (tool));
}

static void
gimp_paint_tool_modifier_key (GimpTool        *tool,
                              GdkModifierType  key,
                              gboolean         press,
                              GdkModifierType  state,
                              GimpDisplay     *display)
{
  GimpPaintTool    *paint_tool    = GIMP_PAINT_TOOL (tool);
  GimpDrawTool     *draw_tool     = GIMP_DRAW_TOOL (tool);
  GimpPaintOptions *paint_options = GIMP_PAINT_TOOL_GET_OPTIONS (tool);
  GimpContext      *context       = GIMP_CONTEXT (paint_options);

  GIMP_DRAW_TOOL_CLASS (parent_class)->draw (draw_tool);

  /* toggle paint erase mode if 'Alt' is pressed and the drawable has alpha */
  if (key == GDK_MOD1_MASK && press && paint_tool->has_alpha)
    {
      gimp_context_set_paint_mode (context, GIMP_LAYER_MODE_ERASE);
      paint_tool->erase = TRUE;
    }
  else if (paint_tool->erase && !press)
    {
      gimp_context_set_paint_mode (context, context->prev_paint_mode);
      paint_tool->erase = FALSE;
    }

  if (paint_tool->pick_colors && ! paint_tool->draw_line)
    {
      if ((state & gimp_get_all_modifiers_mask ()) ==
          gimp_get_constrain_behavior_mask ())
        {
          if (! gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
            {
              GimpToolInfo *info = gimp_get_tool_info (display->gimp,
                                                       "gimp-color-picker-tool");

              if (GIMP_IS_TOOL_INFO (info))
                {
                  if (gimp_draw_tool_is_active (draw_tool))
                    gimp_draw_tool_stop (draw_tool);

                  gimp_color_tool_enable (GIMP_COLOR_TOOL (tool),
                                          GIMP_COLOR_OPTIONS (info->tool_options));

                  switch (GIMP_COLOR_TOOL (tool)->pick_target)
                    {
                    case GIMP_COLOR_PICK_TARGET_FOREGROUND:
                      gimp_tool_push_status (tool, display,
                                             _("Click in any image to pick the "
                                               "foreground color"));
                      break;

                    case GIMP_COLOR_PICK_TARGET_BACKGROUND:
                      gimp_tool_push_status (tool, display,
                                             _("Click in any image to pick the "
                                               "background color"));
                      break;

                    default:
                      break;
                    }
                }
            }
        }
      else
        {
          if (gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
            {
              gimp_tool_pop_status (tool, display);
              gimp_color_tool_disable (GIMP_COLOR_TOOL (tool));
            }
        }
    }
}

static void
gimp_paint_tool_cursor_update (GimpTool         *tool,
                               const GimpCoords *coords,
                               GdkModifierType   state,
                               GimpDisplay      *display)
{
  GimpPaintTool      *paint_tool = GIMP_PAINT_TOOL (tool);
  GimpGuiConfig      *config     = GIMP_GUI_CONFIG (display->gimp->config);
  GimpDisplayShell   *shell      = gimp_display_get_shell (display);
  GimpPaintOptions   *options    = GIMP_PAINT_TOOL_GET_OPTIONS (tool);
  GimpCursorModifier  modifier;
  GimpCursorModifier  toggle_modifier;
  GimpCursorModifier  old_modifier;
  GimpCursorModifier  old_toggle_modifier;

  modifier        = tool->control->cursor_modifier;
  toggle_modifier = tool->control->toggle_cursor_modifier;

  old_modifier        = modifier;
  old_toggle_modifier = toggle_modifier;

  if (! gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
    {
      GimpImage    *image     = gimp_display_get_image (display);
      GList        *drawables = gimp_image_get_selected_drawables (image);
      GList        *iter;

      if (! drawables)
        return;

      for (iter = drawables; iter; iter = iter->next)
        {
          GimpDrawable *drawable = iter->data;

          if (gimp_viewable_get_children (GIMP_VIEWABLE (drawable))               ||
              gimp_item_is_content_locked (GIMP_ITEM (drawable), NULL)            ||
              ! gimp_paint_tool_check_alpha (paint_tool, drawable, display, NULL) ||
              ! (gimp_item_is_visible (GIMP_ITEM (drawable)) ||
                 config->edit_non_visible))
            {
              gboolean constrain_only;

              /* Allow pixel-locked layers to be set as sources */
              constrain_only = (state & gimp_get_constrain_behavior_mask () &&
                                ! (state & gimp_get_extend_selection_mask ()));

              if (! (GIMP_IS_SOURCE_TOOL (tool) && constrain_only))
                {
                  modifier        = GIMP_CURSOR_MODIFIER_BAD;
                  toggle_modifier = GIMP_CURSOR_MODIFIER_BAD;
                  break;
                }
            }
        }

      g_list_free (drawables);

      if (options->brush_snap_to_active_path)
        {
          if (gimp_image_pick_vectors (image,
                                      coords->x, coords->y,
                                      FUNSCALEX (shell, PICK_PATH_PROXIMITY),
                                      FUNSCALEY (shell, PICK_PATH_PROXIMITY)))
            {
              gimp_tool_set_cursor (tool, display,
                                    GIMP_CURSOR_MOUSE,
                                    GIMP_TOOL_CURSOR_HAND,
                                    GIMP_CURSOR_MODIFIER_NONE);
              return;
            }
        }

      if (! paint_tool->show_cursor &&
          modifier != GIMP_CURSOR_MODIFIER_BAD)
        {
          if (paint_tool->draw_brush)
            gimp_tool_set_cursor (tool, display,
                                  GIMP_CURSOR_NONE,
                                  GIMP_TOOL_CURSOR_NONE,
                                  GIMP_CURSOR_MODIFIER_NONE);
          else
            gimp_tool_set_cursor (tool, display,
                                  GIMP_CURSOR_SINGLE_DOT,
                                  GIMP_TOOL_CURSOR_NONE,
                                  GIMP_CURSOR_MODIFIER_NONE);
          return;
        }

      gimp_tool_control_set_cursor_modifier        (tool->control,
                                                    modifier);
      gimp_tool_control_set_toggle_cursor_modifier (tool->control,
                                                    toggle_modifier);
    }

  GIMP_TOOL_CLASS (parent_class)->cursor_update (tool, coords, state,
                                                 display);

  /*  reset old stuff here so we are not interfering with the modifiers
   *  set by our subclasses
   */
  gimp_tool_control_set_cursor_modifier        (tool->control,
                                                old_modifier);
  gimp_tool_control_set_toggle_cursor_modifier (tool->control,
                                                old_toggle_modifier);
}

static void
gimp_paint_tool_oper_update (GimpTool         *tool,
                             const GimpCoords *coords,
                             GdkModifierType   state,
                             gboolean          proximity,
                             GimpDisplay      *display)
{
  GimpPaintTool    *paint_tool    = GIMP_PAINT_TOOL (tool);
  GimpDrawTool     *draw_tool     = GIMP_DRAW_TOOL (tool);
  GimpPaintOptions *paint_options = GIMP_PAINT_TOOL_GET_OPTIONS (tool);
  GimpToolOptions  *tool_options  = GIMP_TOOL_GET_OPTIONS (tool);
  GimpPaintCore    *core          = paint_tool->core;
  GimpDisplayShell *shell         = gimp_display_get_shell (display);
  GimpImage        *image         = gimp_display_get_image (display);
  GList            *drawables;
  gchar            *status        = NULL;
  GType             tool_type     = tool_options->tool_info->tool_type;

  if (gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (tool)))
    {
      GIMP_TOOL_CLASS (parent_class)->oper_update (tool, coords, state,
                                                   proximity, display);
      return;
    }

  gimp_draw_tool_pause (draw_tool);

  if (gimp_draw_tool_is_active (draw_tool) &&
      draw_tool->display != display)
    gimp_draw_tool_stop (draw_tool);

  if (tool->display            &&
      tool->display != display &&
      gimp_display_get_image (tool->display) == image)
    {
      /*  if this is a different display, but the same image, HACK around
       *  in tool internals AFTER stopping the current draw_tool, so
       *  straight line drawing works across different views of the
       *  same image.
       */

      tool->display = display;
    }

  drawables = gimp_image_get_selected_drawables (image);

  if ((g_list_length (drawables) == 1 ||
       (g_list_length (drawables) > 1 && paint_tool->can_multi_paint)) &&
      proximity)
    {
      gboolean  constrain_mask = gimp_get_constrain_behavior_mask ();

      core->cur_coords = *coords;

      if (display == tool->display && (state & GIMP_PAINT_TOOL_LINE_MASK))
        {
          /*  If shift is down and this is not the first paint stroke,
           *  draw a line.
           */
          gchar   *status_help;
          gdouble  offset_angle;
          gdouble  xres, yres;

          gimp_display_shell_get_constrained_line_params (shell,
                                                          &offset_angle,
                                                          &xres, &yres);

          gimp_paint_core_round_line (core, paint_options,
                                      (state & constrain_mask) != 0,
                                      offset_angle, xres, yres);

          status_help = gimp_suggest_modifiers (paint_tool->status_line,
                                                constrain_mask & ~state,
                                                NULL,
                                                _("%s for constrained angles"),
                                                NULL);

          status = gimp_display_shell_get_line_status (shell, status_help,
                                                       ". ",
                                                       core->last_coords.x,
                                                       core->last_coords.y,
                                                       core->cur_coords.x,
                                                       core->cur_coords.y);
          g_free (status_help);
          paint_tool->draw_line = TRUE;
        }
      else
        {
          GdkModifierType  modifiers = 0;

          /* HACK: A paint tool may set status_ctrl to NULL to indicate that
           * it ignores the Ctrl modifier (temporarily or permanently), so
           * it should not be suggested.  This is different from how
           * gimp_suggest_modifiers() would interpret this parameter.
           */
          if (paint_tool->status_ctrl != NULL)
            modifiers |= constrain_mask;

          /* show a status message for Alt modifier erasing */
          if (paint_tool->status_alt != NULL     &&
              paint_tool->has_alpha              &&
              tool_type != GIMP_TYPE_ERASER_TOOL &&
              tool_type != GIMP_TYPE_SMUDGE_TOOL)
            modifiers |= GDK_MOD1_MASK;

          /* suggest drawing lines only after the first point is set
           */
          if (display == tool->display)
            modifiers |= GIMP_PAINT_TOOL_LINE_MASK;

          status = gimp_suggest_modifiers (paint_tool->status,
                                           modifiers & ~state,
                                           _("%s for a straight line"),
                                           paint_tool->status_ctrl,
                                           paint_tool->status_alt);
          paint_tool->draw_line = FALSE;
        }

      paint_tool->cursor_x = core->cur_coords.x;
      paint_tool->cursor_y = core->cur_coords.y;

      if (! gimp_draw_tool_is_active (draw_tool))
        gimp_draw_tool_start (draw_tool, display);
    }
  else if (gimp_draw_tool_is_active (draw_tool))
    {
      gimp_draw_tool_stop (draw_tool);
    }

  if (status != NULL)
    {
      gimp_tool_push_status (tool, display, "%s", status);
      g_free (status);
    }
  else
    {
      gimp_tool_pop_status (tool, display);
    }

  GIMP_TOOL_CLASS (parent_class)->oper_update (tool, coords, state, proximity,
                                               display);

  gimp_draw_tool_resume (draw_tool);
  g_list_free (drawables);
}

static void
gimp_paint_tool_draw (GimpDrawTool *draw_tool)
{
  GimpPaintTool    *paint_tool    = GIMP_PAINT_TOOL (draw_tool);
  GimpPaintOptions *paint_options = GIMP_PAINT_TOOL_GET_OPTIONS (paint_tool);
  GimpDisplayShell *shell         = gimp_display_get_shell (draw_tool->display);

  if (paint_tool->active &&
      ! gimp_color_tool_is_enabled (GIMP_COLOR_TOOL (draw_tool)))
    {
      GimpPaintCore  *core       = paint_tool->core;
      GimpCanvasItem *outline    = NULL;
      gboolean        line_drawn = FALSE;
      gdouble         cur_x, cur_y;

      if (GIMP_TOOL_HANDLE_SIZE_SMALL + shell->picked_timer > 5)
        {
          gimp_draw_tool_add_handle (draw_tool,
                                     GIMP_HANDLE_FILLED_CIRCLE,
                                     shell->picked_x, shell->picked_y,
                                     GIMP_TOOL_HANDLE_SIZE_SMALL + shell->picked_timer,
                                     GIMP_TOOL_HANDLE_SIZE_SMALL + shell->picked_timer,
                                     GIMP_HANDLE_ANCHOR_CENTER);

          shell->picked_timer -= 1;

          /* restore state of layer boundary visibility... */
          if (GIMP_TOOL_HANDLE_SIZE_SMALL + shell->picked_timer == 5)
            {
              if (! gimp_display_shell_get_show_layer (shell))
                  gimp_canvas_item_set_visible (shell->layer_boundary, FALSE);
            }
        }

      if (gimp_paint_tool_paint_is_active (paint_tool) &&
          paint_tool->snap_brush)
        {
          cur_x = paint_tool->paint_x;
          cur_y = paint_tool->paint_y;
        }
      else
        {
          cur_x = paint_tool->cursor_x;
          cur_y = paint_tool->cursor_y;

          if (paint_tool->draw_line &&
              ! gimp_tool_control_is_active (GIMP_TOOL (draw_tool)->control))
            {
              GimpCanvasGroup *group;
              gdouble          last_x, last_y;

              last_x = core->last_coords.x;
              last_y = core->last_coords.y;

              group = gimp_draw_tool_add_stroke_group (draw_tool);
              gimp_draw_tool_push_group (draw_tool, group);

              gimp_draw_tool_add_handle (draw_tool,
                                         GIMP_HANDLE_CIRCLE,
                                         last_x, last_y,
                                         GIMP_TOOL_HANDLE_SIZE_CIRCLE,
                                         GIMP_TOOL_HANDLE_SIZE_CIRCLE,
                                         GIMP_HANDLE_ANCHOR_CENTER);

              gimp_draw_tool_add_line (draw_tool,
                                       last_x, last_y,
                                       cur_x, cur_y);

              gimp_draw_tool_add_handle (draw_tool,
                                         GIMP_HANDLE_CIRCLE,
                                         cur_x, cur_y,
                                         GIMP_TOOL_HANDLE_SIZE_CIRCLE,
                                         GIMP_TOOL_HANDLE_SIZE_CIRCLE,
                                         GIMP_HANDLE_ANCHOR_CENTER);

              gimp_draw_tool_pop_group (draw_tool);

              line_drawn = TRUE;
            }
        }

      gimp_paint_tool_set_draw_fallback (paint_tool, FALSE, 0.0);
      gimp_paint_tool_set_draw_circle (paint_tool, FALSE, 0.0);

      if (paint_tool->draw_brush)
        {
          outline = gimp_paint_tool_get_outline (paint_tool,
                                                 draw_tool->display,
                                                 cur_x, cur_y);
        }

      if (outline)
        {
          if (! paint_options->simple_boundary)
            {
              /* this adds an outline of the brush mask to the brush cursor */
              gimp_draw_tool_add_item (draw_tool, outline);
            }

          gimp_draw_brush_cursor (draw_tool, outline);
          g_object_unref (outline);
        }
      else if (paint_tool->draw_fallback)
        {
          /* zero stylus pressure results in no outline, a fallback is drawn */
          gimp_draw_brush_cursor (draw_tool, outline);
        }
      else if (paint_tool->draw_circle)
        {
          gint size = (gint) paint_tool->circle_size;

          /* Ink tool */
          /* draw an indicatory circle */
          gimp_draw_tool_add_arc (draw_tool,
                                  FALSE,
                                  cur_x - (size / 2.0),
                                  cur_y - (size / 2.0),
                                  size, size,
                                  0.0, (2.0 * G_PI));
        }

      if (! outline                   &&
          ! paint_tool->draw_fallback &&
          ! line_drawn                &&
          ! paint_tool->show_cursor   &&
          ! paint_tool->draw_circle)
        {
          /* I am not sure this case can/should ever happen since now we
           * always set the GIMP_CURSOR_SINGLE_DOT when neither pointer
           * nor outline options are checked. Yet let's imagine any
           * weird case where brush outline is wanted, without pointer
           * cursor, yet we fail to draw the outline while neither
           * circle nor fallbacks are requested (it depends on per-class
           * implementation of get_outline()).
           *
           * In such a case, we don't want to leave the user without any
           * indication so we draw a fallback crosshair.
           */
          if (paint_tool->draw_brush)
            gimp_draw_tool_add_handle (draw_tool,
                                       GIMP_HANDLE_CIRCLE,
                                       cur_x, cur_y,
                                       GIMP_TOOL_HANDLE_SIZE_CROSSHAIR,
                                       GIMP_TOOL_HANDLE_SIZE_CROSSHAIR,
                                       GIMP_HANDLE_ANCHOR_CENTER);
        }
    }

  GIMP_DRAW_TOOL_CLASS (parent_class)->draw (draw_tool);
}

static void
gimp_paint_tool_real_paint_prepare (GimpPaintTool *paint_tool,
                                    GimpDisplay   *display)
{
  GimpDisplayShell *shell = gimp_display_get_shell (display);

  gimp_paint_core_set_show_all (paint_tool->core, shell->show_all);
}

static GimpCanvasItem *
gimp_paint_tool_get_outline (GimpPaintTool *paint_tool,
                             GimpDisplay   *display,
                             gdouble        x,
                             gdouble        y)
{
  if (GIMP_PAINT_TOOL_GET_CLASS (paint_tool)->get_outline)
    return GIMP_PAINT_TOOL_GET_CLASS (paint_tool)->get_outline (paint_tool,
                                                                display, x, y);

  return NULL;
}

static gboolean
gimp_paint_tool_check_alpha (GimpPaintTool  *paint_tool,
                             GimpDrawable   *drawable,
                             GimpDisplay    *display,
                             GError        **error)
{
  GimpPaintToolClass *klass = GIMP_PAINT_TOOL_GET_CLASS (paint_tool);

  /* store a 'has alpha' boolean for the paint erasing toggle */
  if (GIMP_IS_LAYER (drawable)           &&
      gimp_drawable_has_alpha (drawable) &&
      ! gimp_layer_is_alpha_locked (GIMP_LAYER (drawable),NULL))
    {
      paint_tool->has_alpha = TRUE;
    }
  else
    {
      paint_tool->has_alpha = FALSE;
    }

  if (klass->is_alpha_only && klass->is_alpha_only (paint_tool, drawable))
    {
      GimpLayer *locked_layer = NULL;

      if (! gimp_drawable_has_alpha (drawable))
        {
          g_set_error_literal (
            error, GIMP_ERROR, GIMP_FAILED,
            _("The selected drawable does not have an alpha channel."));

          return FALSE;
        }

        if (GIMP_IS_LAYER (drawable) &&
            gimp_layer_is_alpha_locked (GIMP_LAYER (drawable),
                                        &locked_layer))
        {
          g_set_error_literal (
            error, GIMP_ERROR, GIMP_FAILED,
            _("The selected layer's alpha channel is locked."));

          if (error)
            gimp_tools_blink_lock_box (display->gimp, GIMP_ITEM (locked_layer));

          return FALSE;
        }
    }

  return TRUE;
}

static void
gimp_paint_tool_hard_notify (GimpPaintOptions *options,
                             const GParamSpec *pspec,
                             GimpPaintTool    *paint_tool)
{
  if (paint_tool->active)
    {
      GimpTool *tool = GIMP_TOOL (paint_tool);

      gimp_tool_control_set_precision (tool->control,
                                       options->hard ?
                                       GIMP_CURSOR_PRECISION_PIXEL_CENTER :
                                       GIMP_CURSOR_PRECISION_SUBPIXEL);
    }
}

static void
gimp_paint_tool_cursor_notify (GimpDisplayConfig *config,
                               GParamSpec        *pspec,
                               GimpPaintTool     *paint_tool)
{
  gimp_draw_tool_pause (GIMP_DRAW_TOOL (paint_tool));

  paint_tool->show_cursor = config->show_paint_tool_cursor;
  paint_tool->draw_brush  = config->show_brush_outline;
  paint_tool->snap_brush  = config->snap_brush_outline;

  gimp_draw_tool_resume (GIMP_DRAW_TOOL (paint_tool));
}

void
gimp_paint_tool_set_active (GimpPaintTool *tool,
                            gboolean       active)
{
  g_return_if_fail (GIMP_IS_PAINT_TOOL (tool));

  if (active != tool->active)
    {
      GimpPaintOptions *options = GIMP_PAINT_TOOL_GET_OPTIONS (tool);

      gimp_draw_tool_pause (GIMP_DRAW_TOOL (tool));

      tool->active = active;

      if (active)
        gimp_paint_tool_hard_notify (options, NULL, tool);

      gimp_draw_tool_resume (GIMP_DRAW_TOOL (tool));
    }
}

/**
 * gimp_paint_tool_enable_color_picker:
 * @tool:   a #GimpPaintTool
 * @target: the #GimpColorPickTarget to set
 *
 * This is a convenience function used from the init method of paint
 * tools that want the color picking functionality. The @mode that is
 * set here is used to decide what cursor modifier to draw and if the
 * picked color goes to the foreground or background color.
 **/
void
gimp_paint_tool_enable_color_picker (GimpPaintTool       *tool,
                                     GimpColorPickTarget  target)
{
  g_return_if_fail (GIMP_IS_PAINT_TOOL (tool));

  tool->pick_colors = TRUE;

  GIMP_COLOR_TOOL (tool)->pick_target = target;
}

/**
 * gimp_paint_tool_enable_multi_paint:
 * @tool: a #GimpPaintTool
 *
 * This is a convenience function used from the init method of paint
 * tools that want to allow painting with several drawables.
 **/
void
gimp_paint_tool_enable_multi_paint (GimpPaintTool *tool)
{
  g_return_if_fail (GIMP_IS_PAINT_TOOL (tool));

  tool->can_multi_paint = TRUE;
}

void
gimp_paint_tool_set_draw_fallback (GimpPaintTool *tool,
                                   gboolean       draw_fallback,
                                   gint           fallback_size)
{
  g_return_if_fail (GIMP_IS_PAINT_TOOL (tool));

  tool->draw_fallback = draw_fallback;
  tool->fallback_size = fallback_size;
}

void
gimp_paint_tool_set_draw_circle (GimpPaintTool *tool,
                                 gboolean       draw_circle,
                                 gint           circle_size)
{
  g_return_if_fail (GIMP_IS_PAINT_TOOL (tool));

  tool->draw_circle = draw_circle;
  tool->circle_size = circle_size;
}


/**
 * gimp_paint_tool_force_draw:
 * @tool:
 * @force:
 *
 * If @force is %TRUE, the brush, or a fallback, or circle will be
 * drawn, regardless of the Preferences settings. This can be used for
 * code such as when modifying brush size or shape on-canvas with a
 * visual feedback, temporarily bypassing the user setting.
 */
void
gimp_paint_tool_force_draw (GimpPaintTool *tool,
                            gboolean       force)
{
  GimpDisplayConfig *display_config;

  g_return_if_fail (GIMP_IS_PAINT_TOOL (tool));

  display_config = GIMP_DISPLAY_CONFIG (GIMP_TOOL (tool)->tool_info->gimp->config);

  if (force)
    tool->draw_brush = TRUE;
  else
    tool->draw_brush = display_config->show_brush_outline;
}

/**
 * a big outline will always get drawn unless a simple boundary is active
 * a brush has an outline generated from its mask, it may be a tiny outline
 * or it may not exist due to scaling, so draw a locator cursor
 * the user may have chosen a simple boundary to represent the outline
 * sometimes, that simple boundary might be tiny and need a locator
 */
static void
gimp_draw_brush_cursor (GimpDrawTool   *draw_tool,
                        GimpCanvasItem *outline)
{
  GimpDisplayShell *shell       = gimp_display_get_shell (draw_tool->display);
  GimpPaintTool    *paint_tool  = GIMP_PAINT_TOOL (draw_tool);
  GimpPaintOptions *options     = GIMP_PAINT_TOOL_GET_OPTIONS (paint_tool);
  gint              finder_size = (MAX (UNSCALEX (shell, FINDER_SIZE), 2));
  gint              brush_size  = (gint) get_dynamic_brush_size (draw_tool);

  if (! outline || should_locate (draw_tool, shell, FINDER_THRESHOLD))
    draw_brush_circle (draw_tool, finder_size);
  else if (options->simple_boundary)
    draw_brush_circle (draw_tool, brush_size);
}

/**
 * also indicate an erasing mode with a dashed circle and a small core circle
 * this is useful to the painter who can tell what mode the paint tool is in
 * from the cursor alone, this makes painting more intuitive
 */
static void
draw_brush_circle (GimpDrawTool *draw_tool,
                   gint          brush_size)
{
  GimpDisplayShell *shell       = gimp_display_get_shell (draw_tool->display);
  gint              finder_size = (MAX (UNSCALEX (shell, FINDER_SIZE), 2));
  gdouble           angle       = (G_PI / 4.0);
  gint              core_size   = (MAX (UNSCALEX (shell, CORE_SIZE), 2));

  if (is_erasing_paint (draw_tool))
    {
      gimp_draw_arc_circle (draw_tool,
                            brush_size,
                            CIRCLE_DASH_COUNT,
                            CIRCLE_DASH_SPACE,
                            angle);

      draw_circle (draw_tool, finder_size / 2, TRUE);
    }
  else
    {
      /* the outer circle */
      draw_circle (draw_tool, brush_size, FALSE);

      /* the inner core, on mid tone backgrounds, the outer circle can vanish */
      draw_circle (draw_tool, core_size, TRUE);
    }
}

static void
gimp_draw_arc_circle (GimpDrawTool *draw_tool,
                      gint          size,
                      gint          arc_count,
                      gdouble       spacing,
                      gdouble       offset)
{
  GimpPaintTool *paint_tool  = GIMP_PAINT_TOOL (draw_tool);
  gdouble        draw_point  = (2 * G_PI) / arc_count;
  gdouble        slice_angle;
  gdouble        start_angle;

  /* invert for ease of use */
  spacing = 1.0 - spacing;

  /* offset by 90 degrees, so cursor starts at 12 o'clock, 3 arcs test */
  offset = (G_PI / 2) + offset;

  /* the slice of a circle to draw as an arc */
  slice_angle = ( (2 * G_PI) / arc_count ) * spacing;

  /* divide by two, to position half an arc either side of the draw point */
  start_angle = (slice_angle / 2.0);

  for (int i = 0; i < arc_count; i++)
    {
       gimp_draw_tool_add_arc (draw_tool, FALSE,
                               paint_tool->cursor_x - ( size / 2.0 ),
                               paint_tool->cursor_y - ( size / 2.0 ),
                               size, size,
                               ((i * draw_point) - start_angle) + offset,
                               slice_angle);
    }
}

static void
draw_circle (GimpDrawTool *draw_tool,
             gint          size,
             gboolean      filled)
{
  GimpPaintTool *paint_tool = GIMP_PAINT_TOOL (draw_tool);

  gimp_draw_tool_add_arc (draw_tool,
                          filled,
                          paint_tool->cursor_x - (size / 2.0),
                          paint_tool->cursor_y - (size / 2.0),
                          size, size,
                          0.0, (2.0 * G_PI));
}

static gdouble
get_dynamic_brush_size (GimpDrawTool *draw_tool)
{
  GimpPaintTool      *paint_tool = GIMP_PAINT_TOOL (draw_tool);
  GimpPaintOptions   *options    = GIMP_PAINT_TOOL_GET_OPTIONS (paint_tool);
  GimpBrushCore      *brush_core = GIMP_BRUSH_CORE (paint_tool->core);
  GimpDynamicsOutput *dyn_active;
  gdouble             dyn_size;
  gdouble             size_scaled;

  /* is dynamic size used? */
  dyn_active = gimp_dynamics_get_output (brush_core->dynamics,
                                          GIMP_DYNAMICS_OUTPUT_SIZE);
  dyn_size = gimp_dynamics_output_get_linear_value (dyn_active,
                                                    &paint_tool->core->cur_coords,
                                                    options,
                                                    1.0);

  if (gimp_dynamics_output_is_enabled (dyn_active))
    size_scaled = options->brush_size * dyn_size;
  else
    size_scaled = options->brush_size;

  return size_scaled;
}


static gboolean
should_locate (GimpDrawTool     *draw_tool,
               GimpDisplayShell *shell,
               gint              small_brush_threshold)
{
  GimpPaintTool    *paint_tool = GIMP_PAINT_TOOL (draw_tool);
  GimpPaintOptions *options    = GIMP_PAINT_TOOL_GET_OPTIONS (paint_tool);

  /* pressure detected, drawing a stroke, not locating */
  if (paint_tool->core->cur_coords.pressure > EPSILON
     && paint_tool->core->cur_coords.pressure < 1.0)
    {
      return FALSE;
    }

  /* the brush 'screen' size is less than the threshold, locating */
  if (SCALEX (shell, get_dynamic_brush_size (draw_tool)) < small_brush_threshold)
    {
      return TRUE;
    }

  /* pressure zero, brush option size is less than threshold, fallback, locating */
  if (paint_tool->core->cur_coords.pressure <= EPSILON              &&
      (SCALEX (shell, options->brush_size) <= small_brush_threshold &&
      paint_tool->draw_fallback))
    {
      return TRUE;
    }

  return FALSE;
}

gboolean
is_erasing_paint (GimpDrawTool *draw_tool)
{
  GimpToolOptions *tool_options = GIMP_TOOL_GET_OPTIONS (GIMP_TOOL (draw_tool));
  GimpImage       *image        = gimp_display_get_image (draw_tool->display);
  GimpContext     *context      = gimp_get_user_context (image->gimp);

  if (tool_options->tool_info->tool_type == GIMP_TYPE_ERASER_TOOL ||
      gimp_context_get_paint_mode (context) == GIMP_LAYER_MODE_ERASE)
    return TRUE;

  return FALSE;
}

static void
gimp_paintbrush_dab (GimpPaintTool *paint_tool,
                     guint32        time)
{
  GimpPaintCore       *paint_core     = paint_tool->core;
  GimpBrushCore       *core           = GIMP_BRUSH_CORE (paint_core);
  GimpPaintOptions    *paint_options  = GIMP_PAINT_TOOL_GET_OPTIONS (paint_tool);
  GimpBrushDabOptions *dab_options    = paint_options->brush_dab_options;
  GimpContext         *context        = GIMP_CONTEXT (paint_options);
  gdouble              opacity        = context->opacity;
  gdouble              brush_force    = paint_options->brush_force;
  gdouble              brush_angle    = paint_options->brush_angle;
  gdouble              brush_size     = paint_options->brush_size;
  gdouble              min_angle      = dab_options->dab_min_angle;
  gdouble              max_angle      = dab_options->dab_max_angle;
  gboolean             pressure_size  = dab_options->pressure_size;
  gboolean             dyn_on         = FALSE;
  GimpDynamics        *dynamics       = NULL;
  GimpCoords           cur_coords;
  GimpCoords           dab_coords;
  gdouble              brush_pressure;
  gdouble              force_pressure;
  GimpSymmetry        *sym;
  GimpImage           *image;

  /* gather data */
  gimp_paint_core_get_current_coords (paint_core, &cur_coords);
  gimp_paint_core_get_dab_coords (paint_core, &dab_coords);

  /* find the symmetry data, context->image no good ? */
  image = gimp_item_get_image (GIMP_ITEM (paint_tool->drawables->data));
  sym = g_object_ref (gimp_image_get_active_symmetry (image));

  /* scale the stylus tap, to allow for gentle dabbing */
  dab_coords.pressure = MAX(dab_options->dab_min_pressure, dab_coords.pressure);
  brush_pressure  = dab_options->dab_scale_pressure * dab_coords.pressure;
  force_pressure  = dab_options->dab_scale_force * dab_coords.pressure;

  /* normalise dab GUI angle and put in the same range as the brush */
  min_angle = -min_angle / 360.0;
  max_angle = -max_angle / 360.0;

  /* turn off dynamics for dabbing, did this to avoid confusion, however     */
  /* random colour was lost, so just leave dynamics on for now               */

#ifdef SKIP
  if (gimp_paint_options_are_dynamics_enabled (paint_options))
    {
      GimpDynamics *dynamics_off;
      dyn_on = TRUE;
      dynamics = gimp_context_get_dynamics (context);
      dynamics_off = GIMP_DYNAMICS (gimp_dynamics_new (context, "Dynamics Off"));
      gimp_brush_core_set_dynamics (core, dynamics_off);
      g_object_unref (dynamics_off);
    }
#endif

  for (GList *iter = paint_tool->drawables; iter; iter = iter->next)
    {
      gint i;

      /* for every brush stamp */
      for (i = 0; i < dab_options->dab_count; i++)
        {
          gdouble angle_offset  = 0.0;
          gdouble rand_size     = 0.0;
          gdouble rand_pressure = g_random_double_range (dab_options->dab_min_pressure,
                                                         dab_options->dab_max_pressure);
          gdouble dab_pressure  = MIN(brush_pressure * rand_pressure, 1.0);

          /* apply a dynamic pressure and random brush opacity */
          context->opacity = opacity * dab_pressure;

          /* apply a dynamic pressure and random brush force ? */
          if (brush_force != 0.5)
            {
              gdouble rand_force = g_random_double_range (dab_options->dab_min_force,
                                                          dab_options->dab_max_force);
              gdouble dab_force  = MIN(force_pressure * rand_force, 1.0);

              paint_options->brush_force = brush_force * dab_force;
            }

          /* apply the random angle and dynamic tilt angle to the brush angle */
          angle_offset  = g_random_double_range (min_angle, max_angle);
          angle_offset -= paint_options->brush_angle;
          angle_offset += get_tilt_angle(&cur_coords);

          /* wrap the angle offset within the range [-0.5, 0.5) */
          angle_offset = fmod(angle_offset + 0.5, 1.0) - 0.5;

          /* apply the calculated brush angle to the brush */
          paint_options->brush_angle = angle_offset;

          /* apply a random size to the brush */
          rand_size = g_random_double_range (brush_size * dab_options->dab_min_scale,
                                             brush_size * dab_options->dab_max_scale);

          if (pressure_size)
            rand_size *= brush_pressure;

          paint_options->brush_size = rand_size;

          /* apply jitter values to the brush */
          if (dab_options->dab_jitter)
            {
              point circular_rand = circular_random_point(dab_options->dab_jitter);
              cur_coords.x += circular_rand.x;
              cur_coords.y += circular_rand.y;
              gimp_symmetry_set_origin (sym, iter->data, &cur_coords);

              /* restore the current cords to the initial dab position */
              cur_coords.x = paint_core->last_coords.x;
              cur_coords.y = paint_core->last_coords.y;
            }

          /* paint the dab */
          _gimp_paintbrush_motion (paint_core, iter->data, paint_options,
                                   sym,
                                   context->opacity,
                                   paint_options->brush_force);

          /* restore randomised values */
          context->opacity = opacity;
          paint_options->brush_force = brush_force;
          paint_options->brush_size  = brush_size;
          paint_options->brush_angle = brush_angle;
        }

      /* restore dynamics */
      if (dyn_on)
        gimp_brush_core_set_dynamics (core, dynamics);
    }
}

static point
circular_random_point (gdouble radius)
  {
    gdouble angle    = 0;
    gdouble distance = 1;
    point   randomPoint;

    angle    = (g_random_double_range(0 , 2 * G_PI));
    distance = radius * sqrt(g_random_double_range(0 , radius));
    randomPoint.x = distance * cos(angle);
    randomPoint.y = distance * sin(angle);

    return randomPoint;
}

/* converts a stylus tilt into a brush angle */
static gdouble
get_tilt_angle (const GimpCoords *coords)
{
  gdouble angle = 0.0;

  /* pure up or down, or is a calculation required ? */
  if (coords->xtilt == 0.0)
    angle = (coords->ytilt > 0.0) ? 0.25 : 0.75;
  else
    angle = atan (-coords->ytilt / coords->xtilt) / (2 * G_PI);

  /* mirror the angle? flip it over the y axis, for full 360 */
  if (coords->xtilt > 0.0)
    angle += 0.5;

  /* flip the angle 180, a correction to match the brush angle */
  angle -= 0.5;

  /* ensure angle is within the range [0, 1] */
  angle -= floor(angle);

  return angle;
}
